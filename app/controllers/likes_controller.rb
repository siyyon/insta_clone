class LikesController < ApplicationController
  before_action :logged_in_user

  def create
    micropost = Micropost.find(params[:liked_id])
    micropost.liked(current_user)
    # current_user.like(micropost)
    # redirect_to 
  end

  def destroy
    micropost = Like.find(params[:id]).micropost
    current_user.not_like(micropost)
    # redirect_to
  end
end
