# require "pry"

class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @micropost = current_user.microposts.build if logged_in?
      # binding.pry
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end
  
  def about
  end

  def support
  end

  def recruit
  end

  def privacy
  end

  def term
  end

  def language
  end
  
end