Rails.application.routes.draw do

  get 'posts/search'

  # get 'sessions/new'

  root    "static_pages#home"
  get    '/about',    to:'static_pages#about'
  get    '/support',  to:'static_pages#support'
  get    '/recruit',  to:'static_pages#recruit'
  get    '/privacy',  to:'static_pages#privacy'
  get    '/term',     to:'static_pages#term'
  get    '/language', to:'static_pages#language'
  get    '/signup',   to:'users#new'
  post   '/',         to:'users#create'
  get    '/login',    to:'sessions#new'
  delete '/logout',   to:'sessions#destroy'
  post   '/login',    to:'sessions#create'

  resources :users do
    member do
      get :following, :followers, :like
    end
  end
  
  resources :microposts,          only: [:index, :create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :likes,               only: [:create, :destroy]
  
  #get|post user_facebook_omniauth_callback_path -> users/omniauth_callbacks#facebook  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
