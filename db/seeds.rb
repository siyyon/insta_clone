User.create!(email: "example@railstutorial.org",
             full_name:  "Example full_name",
             user_name:  "Example User",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

99.times do |n|
  email = "example-#{n+1}@subtask.org"
  f_name = Faker::Name.name
  u_name = Faker::Name.name
  password = "password"
  User.create!(email: email,
               full_name: f_name,
               user_name: u_name,
               password:              password,
               password_confirmation: password)
end

users = User.order(:created_at).take(6)
50.times do
  # Lorem.sentence: Faker gem のメソッド
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

# Like or not
microposts = Micropost.all
micropost  = microposts.first
liked = microposts[2..50]
like  = users[3..40]
liked.each { |liked| user.like(liked) }